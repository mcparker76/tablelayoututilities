/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.table_layout_utilities;

import java.util.Map;

import layout.TableLayout;

/**
 * Utility class for Table layouts
 * @author Matt Parker
 *
 */
public abstract class TableLayoutUtilities {
	
	private TableLayoutUtilities() {
		//no implementation
	}

	/** Space */
	private static final int TABLE_SPACE = 4;
	
	/**
	 * Creates a table layout with default padding
	 * @param columns - number of columns
	 * @param rows - number of rows
	 * @return - TableLayout
	 */
	public static TableLayout createLayout(int columns, int rows) {
		return createLayout(columns, rows, TABLE_SPACE);
	}
	
	/**
	 * Creates a table layout with customizable padding for outside edge.
	 * 
	 * @param columns - number of columns
	 * @param rows - number of rows
	 * @param padding - outside edge padding
	 * @return - TableLayout
	 */
	public static TableLayout createLayout(int columns, int rows, int padding) {
		double[] c = createArray(2 * columns + 1, padding, null);
		double[] r = createArray(2 * rows + 1, padding, null);
		double[][] grid = {c, r};

		return new TableLayout(grid);
	}
	
	/**
	 * Creates a table layout with default padding and customizable column and/or row sizing.
	 * @param columns - number of columns
	 * @param rows - number of rows
	 * @param columnParameters - Map<@Integer, @Double> column size for column index
	 * @param rowParameters - Map<@Integer, @Double> row size for row index
	 * @return - TableLayout
	 */
	public static TableLayout createLayout(int columns, int rows, Map<Integer, Double> columnParameters,
			Map<Integer, Double> rowParameters) {
		double[] c = createArray(2 * columns + 1, TABLE_SPACE, columnParameters);
		double[] r = createArray(2 * rows + 1, TABLE_SPACE, rowParameters);
		double[][] grid = { c, r };

		return new TableLayout(grid);
	}
	
	/**
	 * Creates a table layout with customizable padding for outside edge and customizable column and/or row sizing.
	 * @param columns - number of columns
	 * @param rows - number of rows
	 * @param padding - outside edge padding
	 * @param columnParameters - Map<@Integer, @Double> column size for column index
	 * @param rowParameters - Map<@Integer, @Double> row size for row index
	 * @return = TableLayout
	 */
	public static TableLayout createLayout(int columns, int rows, int padding, Map<Integer, Double> columnParameters,
			Map<Integer, Double> rowParameters) {
		double[] c = createArray(2 * columns + 1, padding, columnParameters);
		double[] r = createArray(2 * rows + 1, padding, rowParameters);
		double[][] grid = { c, r };

		return new TableLayout(grid);
	}
	
	
	private static double[] createArray(int size, int padding, Map<Integer, Double> params) {
		double[] values = new double[size];
		
		values[0] = padding;
		
		for (int i = 1; i < values.length - 1; i += 2) {
			values[i] = params != null && params.containsKey(i) ? params.get(i) : TableLayout.PREFERRED;
			values[i + 1] = TABLE_SPACE;
		}
		
		values[values.length - 1] = padding;
		
		return values;
	}
	
	/**
	 * Get constraints for component that is left aligned and center aligned.
	 * E.G. "1, 1, L, C"
	 * @param column - the column index
	 * @param row - the row index
	 * @return - the constraints 
	 */
	public static String getConstraints(int column, int row) {
		return getConstraints(column, row, HorizontalAlignment.LEFT, VerticalAlignment.CENTER);
	}
	
	/**
	 * Get constraints for component that is center aligned.
	 * E.G. "1, 1, R, C"
	 * @param column - the column index
	 * @param row - the row index
	 * @param horizontalAlignment - the horizontal alignment
	 * @return - the constraints
	 */
	public static String getConstraints(int column, int row, HorizontalAlignment horizontalAlignment) {
		return getConstraints(column, row, horizontalAlignment, VerticalAlignment.CENTER);
	}
	
	/**
	 * Get constrains for component that is left aligned.
	 * E.G. "1, 1, L, T"
	 * @param column - the column index
	 * @param row - the row index
	 * @param verticalAlignment 0 the vertical alignment
	 * @return - the constraints
	 */
	public static String getConstraints(int column, int row, VerticalAlignment verticalAlignment) {
		return getConstraints(column, row, HorizontalAlignment.LEFT, verticalAlignment);
	}
	
	/**
	 * Get constraints for component
	 * @param column - the column index
	 * @param row - the row index
	 * @param horizontalAlignment - the horizontal alignment
	 * @param verticalAlignment - the vertical alignment
	 * @return - the constraints
	 */
	public static String getConstraints(int column, int row, 
			HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment) {
		return (2 * column - 1) + "," + (2 * row - 1) + "," + horizontalAlignment.getCode() + "," + verticalAlignment.getCode();
	}

}
