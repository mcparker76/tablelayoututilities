/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.table_layout_utilities;

/**
 * Vertical Alignments for table layouts
 * @author Matt Parker
 *
 */
public enum VerticalAlignment{
	/** bottom aligned */
	BOTTOM("B"),
	/** Top aligned */
	TOP("T"),
	/** Center aligned */
	CENTER("C");
	
	private String code;
	
	private VerticalAlignment(String code){
		this.code = code;
	}
	
	/**
	 * Gets the table layout alignment code
	 * @return - code
	 */
	public String getCode(){
		return code;
	}
	
	@Override
	public String toString() {
		return code;
	}
}