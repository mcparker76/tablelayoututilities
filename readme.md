The TableLayoutUtilities is a wrapper for Oracle's TableLayout jar: https://www.oracle.com/java/technologies/tablelayout.html.

Provides easy functionality to create table layouts given number of columns and number of rows.